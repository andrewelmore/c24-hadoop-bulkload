package biz.c24.io.hadoop.samples.bulkload.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RawLocalFileSystem;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.junit.Before;
import org.junit.Test;

import biz.c24.io.hadoop.ComplexDataObjectRecordReader;
import biz.c24.io.swift2012.MT541Message;

public class BulkLoadTest {
    
    private Configuration conf;
    
    @Before
    public void setup() throws IOException, URISyntaxException {
        conf = new Configuration(false);
        conf.set("fs.default.name", "file:///");

        //setup default fs
        RawLocalFileSystem localfs = new RawLocalFileSystem();
        final FileSystem defaultfs = new LocalFileSystem(localfs);
        defaultfs.initialize(new URI("file:///"), conf);
        FileSystem.addFileSystemForTesting(new URI("file:///"), conf, defaultfs);
        
        conf.set("c24.inputformat.element", "biz.c24.io.swift2012.MT541Element");
        conf.set("c24.inputformat.startpattern", "^\\{1:.*");
        conf.set("c24.inputformat.stoppattern", "^-\\}.*");
    }
    
    @Test
    public void testSingleParse() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<MT541Message> reader = new ComplexDataObjectRecordReader<MT541Message>();
        
        FileSplit split = new FileSplit(new Path("data/swift-mt541-100000.dat"), 0, 1000, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // File contains 1 record
        assertTrue(reader.nextKeyValue());

        assertFalse(reader.nextKeyValue()); 
    }
    
    @Test
    public void testMultiParse() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<MT541Message> reader = new ComplexDataObjectRecordReader<MT541Message>();
        
        FileSplit split = new FileSplit(new Path("data/all.dat"), 0, 100000, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // File contains 1000 records
        // Check we can read at least 3
        assertTrue(reader.nextKeyValue());
        assertTrue(reader.nextKeyValue());        
        assertTrue(reader.nextKeyValue());

    }
}