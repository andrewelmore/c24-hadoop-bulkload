package biz.c24.io.hadoop.samples.bulkload;

import java.io.IOException;

import org.apache.hadoop.io.Text;

import biz.c24.io.api.ParserException;
import biz.c24.io.api.data.ValidationException;
import biz.c24.io.hadoop.ComplexDataObjectMapper;
import biz.c24.io.swift2012.MT541Message;

public class DataLoadMapper extends ComplexDataObjectMapper<Text, MT541Message, Text, Text>{
    
    @Override
    protected void handleException(Text key, ParserException ex, Context context) throws IOException, InterruptedException {
        // Send back the exception
        context.write(key, new Text(ex.getMessage()));
    }
    
    @Override
    protected void handleException(Text key, ValidationException ex, Context context) throws IOException, InterruptedException {
        // Send back the exception
        context.write(key, new Text(ex.getReason()));
    }
    
    @Override
    protected void map(Text key, MT541Message value, Context context) throws IOException, InterruptedException {
        try {
            // Insert into data store
            // ...
        } catch(Exception ex) {
            context.write(key, new Text(ex.getMessage()));
        }
    }

}
