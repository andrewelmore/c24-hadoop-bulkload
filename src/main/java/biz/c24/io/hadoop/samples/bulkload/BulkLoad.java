package biz.c24.io.hadoop.samples.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.c24.io.hadoop.ComplexDataObjectCombineFileInputFormat;
import biz.c24.io.hadoop.ComplexDataObjectFileInputFormat;

public class BulkLoad {
    private static Logger LOG = LoggerFactory.getLogger(BulkLoad.class);
    
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        // What we're going to parse
        conf.set("c24.inputformat.element", "biz.c24.io.swift2012.MT541Element");
        // Turn on validation
        conf.setBoolean("c24.inputformat.validate", true);
        // Tell the parser how it can split up messages where there are multiple messages per file
        conf.set("c24.inputformat.startpattern", "^\\{1:.*");
        conf.set("c24.inputformat.stoppattern", "^-\\}.*");
        // Allow mapreduce to run multiple tasks within the same JVM
        conf.setInt("mapred.job.reuse.jvm.num.tasks", -1);
        conf.setInt("mapreduce.job.jvm.numtasks", -1);

        String inputDir = args[0];
        String outputDir = args[1];
        
        Job job = new Job(conf, "C24 Hadoop Bulk Loader");
        job.setInputFormatClass(ComplexDataObjectFileInputFormat.class);
        job.setJarByClass(BulkLoad.class);
        job.setMapperClass(DataLoadMapper.class);
        // Since we want all errors back, use the identity reducer
        job.setCombinerClass(Reducer.class);
        job.setReducerClass(Reducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        ComplexDataObjectFileInputFormat.addInputPath(job, new Path(inputDir));
        FileOutputFormat.setOutputPath(job, new Path(outputDir));
 
        
        
        System.exit(job.waitForCompletion(true) ? 0 : 1);
      }

}
